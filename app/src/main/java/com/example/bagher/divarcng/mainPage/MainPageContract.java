package com.example.bagher.divarcng.mainPage;

import com.example.bagher.divarcng.models.MVC;

import java.util.List;

public interface MainPageContract {

    interface View {
        void setAdapter(List<MVC> list);
        void noInternet();
        void progressBar();
        void progressBarTwo();
        void toast(String message);
    }

    interface Presenter {
        void advertisingRefresh();
        void advertisingAdd();
    }

}

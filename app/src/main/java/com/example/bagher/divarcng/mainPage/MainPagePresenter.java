package com.example.bagher.divarcng.mainPage;


import com.example.bagher.divarcng.models.MVC;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.TextHttpResponseHandler;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import cz.msebera.android.httpclient.Header;

public class MainPagePresenter implements MainPageContract.Presenter {
    MainPageContract.View view;
    List <MVC> advertising;

    public MainPagePresenter(MainPageContract.View view) {
        this.view = view;
    }

    @Override
    public void advertisingRefresh() {
        searchAsyncHttpClient();
    }

    @Override
    public void advertisingAdd() {
        searchAsyncHttpClient();
    }

    private void searchAsyncHttpClient() {
        String url = "http://www.mocky.io/v2/5a781dea2f00000f00668e3e";
        AsyncHttpClient clientC = new AsyncHttpClient();
        view.progressBar();
        clientC.get( url, new TextHttpResponseHandler() {
            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                view.noInternet();
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, String responseString) {
                jsonFinal( responseString );
            }

            @Override
            public void onFinish() {
                super.onFinish();
                view.progressBarTwo();
            }
        } );
    }

    private void jsonFinal(String responseString) {
        Gson gson = new Gson();
        Type collectionType = new TypeToken <List <MVC>>() {
        }.getType();
        advertising = gson.fromJson( responseString, collectionType );
        view.setAdapter( advertising );
    }

}
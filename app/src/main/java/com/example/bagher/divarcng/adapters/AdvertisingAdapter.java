package com.example.bagher.divarcng.adapters;


import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.bagher.divarcng.R;
import com.example.bagher.divarcng.models.MVC;
import com.squareup.picasso.Picasso;

import java.util.List;


public class AdvertisingAdapter extends BaseAdapter {
    private Context mContext;
    private List <MVC> listMvc;

    public AdvertisingAdapter(Context mContext, List <MVC> listMvc) {
        this.mContext = mContext;
        this.listMvc = listMvc;
    }



    @Override
    public int getCount() {
        return listMvc.size();
    }

    @Override
    public Object getItem(int position) {
        return listMvc.get( position );
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View view, ViewGroup viewGroup) {
        @SuppressLint("ViewHolder") View rowView = LayoutInflater.from( mContext ).inflate( R.layout.advertising_list_item, viewGroup, false );
        MVC mean = (MVC) getItem( position );
        ImageView imageAdvertising = rowView.findViewById( R.id.imageAdvertising );
        TextView nameAdvertising = rowView.findViewById( R.id.nameAdvertising );
        TextView timeAdvertising = rowView.findViewById( R.id.timeAdvertising );
        TextView priceAdvertising = rowView.findViewById( R.id.priceAdvertising );
        Picasso.with( mContext ).load( mean.getImage().get( 0 ).getAddres() ).into( imageAdvertising );
        nameAdvertising.setText( mean.getId() );
        timeAdvertising.setText( mean.getName() );
        priceAdvertising.setText( mean.getId() );
        return rowView;
    }
}

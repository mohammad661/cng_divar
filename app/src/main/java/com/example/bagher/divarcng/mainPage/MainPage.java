package com.example.bagher.divarcng.mainPage;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.widget.DrawerLayout;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AbsListView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.example.bagher.divarcng.BaseActivity;
import com.example.bagher.divarcng.R;
import com.example.bagher.divarcng.adapters.AdvertisingAdapter;
import com.example.bagher.divarcng.models.MVC;

import java.util.ArrayList;
import java.util.List;

public class MainPage extends BaseActivity implements MainPageContract.View, SwipeRefreshLayout.OnRefreshListener {
    MainPageContract.Presenter presenter;
    DrawerLayout drawerLayout;
    ImageView openDrawerLayout;
    ListView listViewAll;
    SwipeRefreshLayout swipe;
    TextView bagher;
    ProgressBar progressBar;
    AdvertisingAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate( savedInstanceState );
        setContentView( R.layout.activity_main_page );
        bindView();
        swipe.setOnRefreshListener( this );
        presenter.advertisingRefresh();
    }

    @SuppressLint("InflateParams")
    private void bindView() {
        presenter = new MainPagePresenter( this );
        drawerLayout = findViewById( R.id.drawerLayout );
        openDrawerLayout = findViewById( R.id.openDrawerLayout );
        listViewAll = findViewById( R.id.listViewAll );
        swipe = findViewById( R.id.swipe );
        bagher = findViewById( R.id.bagher );
        progressBar = findViewById( R.id.progressBar );
    }

    @Override
    public void onRefresh() {
        presenter.advertisingAdd();
        swipe.setRefreshing( false );
    }

    @Override
    public void setAdapter(List <MVC> list) {
        adapter = new AdvertisingAdapter( this, list );
        listViewAll.setAdapter( adapter );
    }

    @Override
    public void noInternet() {
        bagher.setVisibility( View.VISIBLE );
        listViewAll.setVisibility( View.INVISIBLE );
    }

    @Override
    public void progressBar() {
        progressBar.setVisibility( View.VISIBLE );
    }

    @Override
    public void progressBarTwo() {
        progressBar.setVisibility( View.INVISIBLE );
    }

    @Override
    public void toast(String message) {
        Toast.makeText( this, message, Toast.LENGTH_SHORT ).show();
    }

}
